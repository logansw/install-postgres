#!/usr/bin/bash
if [ ! -f postgres.args ]; then
   echo "*** Please run install_postgres.sh first before attempting to generate a pkg manifest for  Postgres!!!"
   exit
fi

source postgres.args
cp /usr/local/src/$PGDIR/COPYRIGHT  $PGPATH/$VERSION/share/
DBBASEDIR=/var/postgres
MANIFEST_PROTO_DIR=/tmp/proto$SHORTVER
mkdir -p $MANIFEST_PROTO_DIR${PGPATH}/$VERSION
./configure_pg_smf.sh -g 
(export PATH=$PATH:${PGPATH}/$VERSION/bin/; cd oracle-fdw && gmake clean && gmake ORACLE_SHLIB="clntsh -lnnz11 -R$ORACLE_HOME" && gmake install)
ESCAPE_CWD=$(pwd | sed 's_/_\\/_g')
#CWD_REPLACE replaces %%%%%% in the p5i pkg template with the directory of this script
CWD_REPLACE="s/%%%%%%/$ESCAPE_CWD/g"

#VER_REPLACE replaces ###### with the postgres version number e.g. 9.5.2
VER_REPLACE="s/######/$VERSION/g"

#TWODIGIT_REPLACE replaces :::::: with the first two digits of the postgres version e.g. 9.5
TWODIGIT_REPLACE="s/::::::/$TWODIGITVER/g"

#UNFORMAT_REPLACE replaces @@@@@@ with the postgres version sans formatting e.g. 952
UNFORMAT_REPLACE="s/@@@@@@/$SHORTVER/g"

TPL=$(cat pg-pkg-template.p5i | sed  $VER_REPLACE | sed  $TWODIGIT_REPLACE | sed  $UNFORMAT_REPLACE | sed  $CWD_REPLACE )
(cd $PGPATH/$VERSION && gtar cf - . | (cd $MANIFEST_PROTO_DIR${PGPATH}/$VERSION && gtar xf - ))
MANIFEST_TXT=$(pkgsend generate $MANIFEST_PROTO_DIR )
echo "$TPL\n$MANIFEST_TXT" | pkgfmt -u > $MANIFEST_PROTO_DIR/postgres-${VERSION}.p5i 
cd $MANIFEST_PROTO_DIR
pkgsend publish -s http://catena.usurf.usu.edu:8001 postgres-${VERSION}.p5i
