#!/usr/bin/env bash
CURR_DIR=$(pwd)
PGCC="CC=\"gcc -m64\""
ARCH=64
DTRACE_SUPPORT="--enable-dtrace DTRACEFLAGS='-64'"
while [ $# -gt 0 ]; do
  case "$1" in
    --version*)
      VER=${1#*=}
      ;;
    --port*)
      PORT=${1#*=}
      ;;
    --path*)
      PGPATH=${1#*=}
      ;;
    --subdir*)
      SUBDIR=${1#*=}
      ;;
    --conf-inc-dir*)
      CONFINCDIR=${1#*=}
      ;;
    --32)
      PGCC="CC='gcc -O3 '"
      ARCH=32
      DTRACE_SUPPORT=""
      ;;
    --help)
         cat << EOF

 Usage: --version=<version_number> --path=<directory> [--port=<port>] [--32] | --help
         [...] denotes an optional argument
         --version        Sets the desired version of postgres
         --path           Specify the install path prefix, default is /usr/local/postgres
                          final path would be <prefix>/<subdir> e.g. /usr/local/postgres/10.5
         --conf-inc-dir   Specify the directory where the postgres configuration include files
                          will be located. Defaults to /usr/local/etc/postgres
         --subdir         Provide the name that will be used for the postgres data subdirectory
         --port           Specify a port other than 5432 (Optional)
         --32             Enable 32 bit compile, disable 64 bit support (Optional)
         --help           Displays this help message
EOF
         exit
         break;;
  esac
shift
done


if [ ! $VER ]
   then
   echo "A value for --version is required. Use the following format  #.# || #.#.rc#"
   exit
fi
set -- `echo $VER | tr '.' ' '`
VER_MAJ=$1
VER_MIN=$2
VER_RC=$3

if [ ! $PORT ]
   then
      PORT=5432
fi
if [ ! $PGPATH ]
   then
      PGPATH=/usr/local/postgres
fi
if [ ! $CONFINCBASE ]
   then
      CONFINCBASE=/usr/local/etc
fi
if [ ! $SUBDIR ]
   then
   echo "A value for --subdir is required. Format like ## (e.g. 10)"
   exit
fi
export PGVERSION=$VER
COMPILEDIR=postgresql-$VER


echo "\n<Verifying existence of /export/home directory...>"
if [ ! -d "/export/home" ]; then
   mkdir -p /export/home
fi

(groupadd -g 90 postgres &>/dev/null && echo "Added group postgres" ) || echo "Group postgres already exists"
(useradd -u 90 -d /export/home/postgres -m -g 90 -s /bin/bash postgres &>/dev/null && echo "Added user postgres" ) || echo "User postgres already exists"

if [ ! -d "/usr/local/src" ]; then
    mkdir -p /usr/local/src/
fi

if [ ! -f "/usr/local/src/postgresql-$VER.tar.gz" ]; then
    echo "<Download Postgres Source>"
    echo "wget -nv --no-check-certificate https://ftp.postgresql.org/pub/source/v$VER/postgresql-$VER.tar.gz -O /usr/local/src/postgresql-$VER.tar.gz >> download.out  "
    wget -nv --no-check-certificate https://ftp.postgresql.org/pub/source/v$VER/postgresql-$VER.tar.gz -O /usr/local/src/postgresql-$VER.tar.gz >> download.out
fi

echo "\n<Changing directory to /usr/local/src/>"
echo "cd /usr/local/src/"
cd /usr/local/src/;

echo "<Extracting Postgres ...>"
if [ ! -d "postgresql-$VER" ]; then
    tar xzf postgresql-$VER.tar.gz
fi


echo "\n<Changing directory to /usr/local/src/$COMPILEDIR>"
echo "cd /usr/local/src/$COMPILEDIR;"
cd /usr/local/src/$COMPILEDIR;

echo "\n<Configuring Postgres... configure.out>"
echo "./configure $PGCC --prefix=$PGPATH/$VER --with-openssl --with-zlib --with-python --with-libxml --with-libedit-preferred --with-pgport=$PORT $DTRACE_SUPPORT > /usr/local/src/$COMPILEDIR/configure.out 2>&1;"
bash -c "./configure $PGCC --prefix=$PGPATH/$VER --with-openssl --with-zlib --with-python --with-libxml --with-libedit-preferred --with-pgport=$PORT $DTRACE_SUPPORT > /usr/local/src/$COMPILEDIR/configure.out 2>&1;"

echo "\n<Compiling Postgres... compile.out>"
echo " /usr/bin/gmake world > /usr/local/src/$COMPILEDIR/compile.out 2>&1;"
/usr/bin/gmake world  > /usr/local/src/$COMPILEDIR/compile.out 2>&1;

echo "\n<Installing Postgres... install.out>"
echo "/usr/bin/gmake install-world > /usr/local/src/$COMPILEDIR/install.out 2>&1"
/usr/bin/gmake install-world > /usr/local/src/$COMPILEDIR/install.out 2>&1

cat  << EOF > $CURR_DIR/postgres.args
VERSION=$VER
VER_MAJ=$VER_MAJ
VER_MIN=$VER_MIN
VER_RC=$VER_RC
SUBDIR=$SUBDIR
PORT=$PORT
PGPATH=$PGPATH
COMPILEDIR=$COMPILEDIR
ARCH=$ARCH
CONFINCBASE=$CONFINCBASE
EOF
