#!/usr/bin/bash

source  postgres.args
CURR_DIR=$(pwd)
while getopts  "d::h" opt; do
   case $opt in
      d)
         DATADIR="$OPTARG"
         ;;
      h)
         echo "Usage: -d <data-dir path>"
         exit
         ;;
      --)
      shift
      break;;
   esac
done
if [ ! $DBBASEDIR ]
   then
      DBBASEDIR=/var/postgres
fi
if [ ! $DATADIR ]
   then
      DATADIR=$DBBASEDIR/$SUBDIR/data
fi

#Establish service and postgres configuration variables necessary to register with SMF

COMPACT_VERSION=$(echo $VERSION | sed -r "s/\.//g")
SERVICE_NAME=postgresql$VER_MAJ
echo "\n<Checking svc manifest dir exists...>"
if [ ! -d "/var/svc/manifest/application/database/" ]; then
    mkdir -p /var/svc/manifest/application/database
fi
echo "\n<Generating SMF Manifest file...>"
cat  << EOF > $CURR_DIR/postgres-manifest.smf
<?xml version="1.0"?>
<!DOCTYPE service_bundle SYSTEM "/usr/share/lib/xml/dtd/service_bundle.dtd.1">
<!--
 Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 Use is subject to license terms.

 ident	"@(#)$SERVICE_NAME.xml	1.1	08/04/30 SMI"

        NOTE:  This service manifest is not editable; its contents will
        be overwritten by package or patch operations, including
        operating system upgrade.  Make customizations in a different
        file.
-->

<service_bundle type='manifest' name='$SERVICE_NAME'>

<service
        name='application/database/$SERVICE_NAME'
        type='service'
        version='1'>

	<!--
	   Wait for network interfaces to be initialized.
	-->
        <dependency
                name='network'
                grouping='require_all'
                restart_on='none'
                type='service'>
                <service_fmri value='svc:/milestone/network:default' />
        </dependency> 

	<!--
	   Wait for all local filesystems to be mounted.
	-->
        <dependency
                name='filesystem-local'
                grouping='require_all'
                restart_on='none'
                type='service'>
                <service_fmri value='svc:/system/filesystem/local:default' />
        </dependency> 

        <exec_method
                type='method'
                name='start'
                exec='/lib/svc/method/postgres_$VERSION start'
                timeout_seconds='60' />

        <exec_method
                type='method'
                name='stop'
                exec='/lib/svc/method/postgres_$VERSION stop'
                timeout_seconds='60' />

        <exec_method
                type='method'
                name='refresh'
                exec='/lib/svc/method/postgres_$VERSION refresh'
                timeout_seconds='60' />

        <!--
          Both action_authorization and value_authorization are needed
          to allow the framework general/enabled property to be changed
          when performing action (enable, disable, etc) on the service.
        -->
        <property_group name='general' type='framework'>
		<propval name='value_authorization' type='astring'
			value='solaris.smf.value.postgres' />
		<propval name='action_authorization' type='astring'
			value='solaris.smf.manage.postgres' />
        </property_group>


        <instance name='v${COMPACT_VERSION}_${ARCH}bit' enabled='false'>

        	<method_context>
                	<method_credential user='postgres' group='postgres' />
        	</method_context>

		<!-- 
		   Make sure the data configurable property points to the
		   appropriate database directory and port number in 
		   postgresql.conf is different than the other instances.
		-->
        	<property_group name='$SERVICE_NAME' type='application'>
                	<propval name='bin' type='astring'
                    	   value='$PGPATH/$VERSION/bin' />
                	<propval name='data' type='astring'
                    	   value='$DATADIR' />
                	<propval name='log' type='astring'
			   value='$DBBASEDIR/$SUBDIR/logs/server.log' />
			<propval name='value_authorization' type='astring'
			   value='solaris.smf.value.postgres' />
        	</property_group>

        </instance>


        <stability value='Evolving' />

        <template>
                <common_name>
                        <loctext xml:lang='C'>
                                PostgreSQL RDBMS version $SERVICE_NAME 
                        </loctext>
                </common_name>
                <documentation>
                        <manpage title='$SERVICE_NAME' section='5' />
                        <doc_link name='postgresql.org'
                                uri='http://postgresql.org' />
                </documentation>
        </template>

</service>
</service_bundle>
EOF

cat  << EOF > /lib/svc/method/postgres_$VERSION
#!/sbin/sh
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)$SERVICE_NAME	1.1	08/04/30 SMI"

. /lib/svc/share/smf_include.sh

# SMF_FMRI is the name of the target service. This allows multiple instances 
# to use the same script.

getproparg() {
        val=\`svcprop -p \$1 \$SMF_FMRI\`
        [ -n "\$val" ] && echo \$val
}

check_data_dir() {
	if [ ! -d \$PGDATA ]; then
		echo "Error: $SUBDIR/data directory \$PGDATA does not exist"
		exit \$SMF_EXIT_ERR_CONFIG
	fi

	if [ ! -w \$PGDATA ]; then
		echo "Error: $SUBDIR/data directory \$PGDATA is not writable by postgres"
		exit \$SMF_EXIT_ERR_CONFIG
	fi

	if [ ! -d \$PGDATA/base -o ! -d \$PGDATA/global -o ! -f \$PGDATA/PG_VERSION ]; then
		# If the directory is empty we can create the database files
		# on behalf of the user using initdb
		if [ \`ls -a \$PGDATA | wc -w\` -le 2 ]; then
			echo "Notice: $VERSION/data directory \$PGDATA is empty"
			echo "Calling '\$PGBIN/initdb -D \$PGDATA' to initialize"

			\$PGBIN/initdb -D \$PGDATA
			if [ \$? -ne 0 ]; then
				echo "Error: initdb failed"
				exit \$SMF_EXIT_ERR
			fi
		else
			echo "Error: $SERVICE_NAME data directory \$PGDATA is not empty, nor is it a valid PostgreSQL data directory"
			exit \$SMF_EXIT_ERR_CONFIG
		fi
	fi
}

PGBIN=\`getproparg $SERVICE_NAME/bin\`
PGDATA=\`getproparg $SERVICE_NAME/data\`
PGLOG=\`getproparg $SERVICE_NAME/log\`

if [ -z \$SMF_FMRI ]; then
	echo "Error: SMF framework variables are not initialized"
	exit \$SMF_EXIT_ERR
fi

if [ -z \$PGDATA ]; then
        echo "Error: $SERVICE_NAME/data property not set"
        exit \$SMF_EXIT_ERR_CONFIG
fi

if [ -z \$PGLOG ]; then
        echo "Error: $SERVICE_NAME/log property not set"
        exit \$SMF_EXIT_ERR_CONFIG
fi


case "\$1" in
'start')
	check_data_dir
        \$PGBIN/pg_ctl -D \$PGDATA -l \$PGLOG -w start
        ;;

'stop')
        \$PGBIN/pg_ctl -D \$PGDATA -m fast stop
        ;;

'refresh')
        \$PGBIN/pg_ctl -D \$PGDATA -m fast reload
        ;;

*)
        echo "Usage: \$0 {start|stop|refresh}"
        exit 1
        ;;

esac
exit \$SMF_EXIT_OK
EOF

manifest_file=${SERVICE_NAME}_v${COMPACT_VERSION}_${ARCH}bit.xml
chmod 755 /lib/svc/method/postgres_$VERSION 
cp $CURR_DIR/postgres-manifest.smf /var/svc/manifest/application/database/$manifest_file
echo "\n<Registering Postgres with SVC...>"
echo "\n<Checking if Postgres service exists...>"
SVC_EXISTS=$(svcs -a | grep "${SERVICE_NAME}:v${COMPACT_VERSION}_${ARCH}bit" | sed -r 's/( )+/ /g' | cut -d' ' -f 3)
if [ $SVC_EXISTS ]; then 

    echo "Service $SVC_EXISTS already exists,\ndeleting service..."
    echo "svccfg delete $SVC_EXISTS"
    svccfg delete $SVC_EXISTS
else
    echo "Service $SERVICE_NAME doesn't exist"
fi

echo "\n<Importing Service SMF for Postgres...>"
echo "svccfg -v import /var/svc/manifest/application/database/$manifest_file"
svccfg -v import /var/svc/manifest/application/database/$manifest_file
echo "\n\n** To enable the postgres service, execute ***"
echo "svcadm enable $SERVICE_NAME"
