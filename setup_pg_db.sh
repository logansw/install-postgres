#!/usr/bin/bash
if [ ! -f postgres.args ]; then
   echo "*** Please run install_postgres.sh before attempting to initialize the database!!!"
   exit
fi

source  postgres.args
if [ ! $PGPATH ]; then
   echo "*** Please run install_postgres.sh before attempting to initialize the database!!!"
   exit
fi
CURR_DIR=$(pwd)
while getopts  "d::h" opt; do
   case $opt in
      d)
         $DATADIR=$OPTARGS
         ;;
      h)
         cat << EOF

 Usage:  -d <directory>  | -h
         -d   specify the postgres data directory path

EOF
         exit
         ;;
      --)
      shift
      break;;
   esac
done

if [ ! $DATADIR ]; then
   DATADIR=/var/postgres/$SUBDIR/data
fi
if [ -d "$DATADIR" ]; then
    echo "\n<Postgres Data Dir $DATADIR already exists>"
else
   echo "\n<Creating Postgres Data Dir...>"
   echo "mkdir -p $DATADIR"
   mkdir -p $DATADIR
fi

if [ -d "$CONFINCBASE" ]; then
    echo "\n<Postgres Data Dir $CONFINCBASE already exists>"
else
    echo "\n Copying postgres config include files to $CONFINCBASE ..."
    mkdir -p $CONFINCBASE
    cp -r postgres-config-include $CONFINCBASE/postgres
    chown -R postgres:postgres $CONFINCBASE/postgres
fi

DBBASEDIR=$(cd $DATADIR && cd ../.. && pwd)
echo "\n<Changing Postgres Base Data Dir Ownership...>"
echo "chown -R postgres:postgres $DBBASEDIR"
chown -R postgres:postgres $DBBASEDIR

echo "\n<Changing Postgres Data Dir Ownership...>"
echo "chown -R postgres:postgres $DATADIR"
chown -R postgres:postgres $DATADIR

echo "\n<Initialize Postgres Data Dir...>"
echo "su - postgres -c \"$PGPATH/$VERSION/bin/initdb -E UTF8 -D $DATADIR\""
su - postgres -c "$PGPATH/$VERSION/bin/initdb -E UTF8 -D $DATADIR"

if [ ! -d "$DBBASEDIR/$SUBDIR/logs" ]; then
    echo "mkdir -p $DBBASEDIR/$SUBDIR/logs"
    su - postgres -c "mkdir -p $DBBASEDIR/$SUBDIR/logs"
fi

if [ -d "$DBBASEDIR/$SUBDIR/wal" ]; then
    echo "moving $DBBASEDIR/$SUBDIR/data/pg_wal files to $DBBASEDIR/$SUBDIR/wal"
    su - postgres -c "mv $DBBASEDIR/$SUBDIR/data/pg_wal/* $DBBASEDIR/$SUBDIR/wal/"
    cd $DBBASEDIR/$SUBDIR/data/
    rm -rf pg_wal
    ln -s $DBBASEDIR/$SUBDIR/wal $DBBASEDIR/$SUBDIR/data/pg_wal
fi

if ! grep -q "${CONFINCBASE}" $DBBASEDIR/$SUBDIR/data/postgresql.conf; then
    sed  -i '/# default postgresql.conf/a\
    include_if_exists = '\'${CONFINCBASE}/postgres/${SUBDIR}.conf\''' $DBBASEDIR/$SUBDIR/data/postgresql.conf
fi

#if [ ! -d "$DATADIR/log" ]; then
#    echo "\nCreate sym-link to logs directory"
#    echo "ln -s $DBBASEDIR/$SUBDIR/logs $DATADIR/log"
#    su - postgres -c "ln -s $DBBASEDIR/$SUBDIR/logs $DATADIR/log"
#fi

echo "DBBASEDIR=$DBBASEDIR" >> postgres.args
echo "\n**[Postgres Install Complete]**\nPlease make sure the following is in your path:\n$PGPATH/$SUBDIR/bin/"
